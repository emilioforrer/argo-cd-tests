

## K8s setup

```bash
kubectl apply -f k8s/manifests/
```

## Export .env file

```bash
# -o allexport enables all following variable definitions to be exported. +o allexport disables this feature. (-a and +a are equivalents)
set -a; [ -f .env ] && source .env; set +a
```

## Replace env vars in value.yaml

```bash
envsubst < k8s/charts/demo-app-chart/values.yaml > k8s/charts/demo-app-chart/values.deploy.yaml
```

## Download Argo CLI

```
curl -sSL -o /usr/local/bin/argocd https://${ARGOCD_SERVER}/download/argocd-linux-amd64
chmod +x /usr/local/bin/argocd
# argocd app set argo-cd-tests --helm-set main.deployment.image.version=latest
argocd app set argo-cd-tests --values-literal-file ./k8s/charts/demo-app-chart/values.deploy.yaml
argocd app sync argo-cd-tests --force --prune
argocd app wait argo-cd-tests  --operation --health --timeout 300
```

## ArgoCD ingres annotations

```
  nginx.ingress.kubernetes.io/backend-protocol: HTTPS
  nginx.ingress.kubernetes.io/force-ssl-redirect: "true"
  nginx.ingress.kubernetes.io/ssl-passthrough: "true"
```

